import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Homepage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.list= [];

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        this.list.push(this.state.value);
        event.preventDefault();
        alert('Are you sure about that?' );
        this.setState({value:''});
    }





    render(){



        return (

            <form onSubmit={this.handleSubmit}>
                <label>
                    Add an element to the list:
                    <input type="text" value={this.state.value} onChange= {this.handleChange} />
                </label>
                <input type="submit" value="Save on the list" />
                <h2>Elements</h2>
                <textarea value={this.list.map((elem, index ) => (index + 1) + ". " + elem + '\n').join(" ")} />




            </form>
        );

    }
}

// ========================================

ReactDOM.render(
    <Homepage/>,
    document.getElementById('root')
);

